<!DOCTYPE html>
<html lang="en">

<head>
<!-- Smartsupp Live Chat script -->
<script type="text/javascript">
var _smartsupp = _smartsupp || {};
_smartsupp.key = '96812bf85b92ca168ff6993db357555d33dd0b84';
window.smartsupp||(function(d) {
  var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
  s=d.getElementsByTagName('script')[0];c=d.createElement('script');
  c.type='text/javascript';c.charset='utf-8';c.async=true;
  c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
})(document);
</script>



    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>test - Home</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- /Preloader -->

    <!-- Top Search Form Area -->
    <div class="top-search-area">
        <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <!-- Close -->
                        <button type="button" class="btn close-btn" data-dismiss="modal"><i class="ti-close"></i></button>
                        <!-- Form -->
                        <form action="index.html" method="post">
                            <input type="search" name="top-search-bar" class="form-control" placeholder="Search and hit enter...">
                            <button type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Header Area Start -->
    <header class="header-area">
        <!-- Main Header Start -->
        <div class="main-header-area">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Classy Menu -->
                    <nav class="classy-navbar justify-content-between" id="alimeNav">

                        <!-- Logo -->
                        <a class="nav-brand" href="./index.html"><img src="./img/core-img/logo.png" alt=""></a>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">
                            <!-- Menu Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>
                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul id="nav">
                                    <li class="active"><a href="./index.php">HOME</a></li>
                                 <li><a href="#">ABOUT US</a></li> 
                                    <li><a href="#">SERVICES</a></li>
                                    <li><a href="#">PORTFOLIO</a></li> 
                                    <li><a href="./contact.php">CONTACT US</a></li>
                                </ul>

                                <!-- Search Icon -->
                               
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Area End -->

    <!-- Welcome Area Start -->

 <!-- <section class="welcome-area">
       <div class="about-video-area mb-80 wow fadeInUp" data-wow-delay="100ms">
                        <img src="img/bg-img/1.jpg" alt=""> -->
                        <!-- <div class="video-icon">
                            <a href="https://www.youtube.com/watch?v=sSakBz_eYzQ" class="video-play-btn"><i class="arrow_triangle-right"></i></a>
                        </div> -->
                    <<!-- /div> -->





    
   <section class="welcome-area">
        <div class="welcome-slides owl-carousel"> 
            <!-- Single Slide -->
     <div class="single-welcome-slide bg-img bg-overlay" style="background-image: url(img/bg-img/1.jpg);"> </div>
     <div class="single-welcome-slide bg-img bg-overlay" style="background-image: url(img/bg-img/1.5.jpg);"> </div>
       
        </div>
    </section> 












   
    <!-- About Us Area End -->

    <!-- Why Choose Us Area Start -->
    <section class="why-choose-us-area  section-padding-80-0 clearfix" style="padding-top: 30px;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center wow fadeInUp  " data-wow-delay="100ms">
                        <h2>why choose us</h2>

                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Why Choose Area -->
                <div class="col-md-6 col-lg-4">
                    <div class="why-choose-us-content text-center  wow fadeInUp" data-wow-delay="100ms">
                        <!-- <div class="chosse-us-icon"> -->
                            <!-- <i class="fa fa-film" aria-hidden="true"></i> -->
                       <!--  </div> -->
                        <h4>Graphic Design</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut elit, sed do eiusmod te</p>
                    </div>
                </div>

                <!-- Single Why Choose Area -->
                <div class="col-md-6 col-lg-4">
                    <div class="why-choose-us-content text-center wow fadeInUp" data-wow-delay="300ms">
                        <!-- <div class="chosse-us-icon">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </div> -->
                        <h4>Social Media Marketing</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut elit, sed do eiusmod te</p>
                    </div>
                </div>

                <!-- Single Why Choose Area -->
                <div class="col-md-6 col-lg-4">
                    <div class="why-choose-us-content text-center  wow fadeInUp" data-wow-delay="500ms">
                        <!-- <div class="chosse-us-icon">
                            <i class="fa fa-camera" aria-hidden="true"></i>
                        </div> -->
                        <h4>Web Development</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut elit, sed do eiusmod te</p>
                    </div>
                </div>
            </div>


  


        </div>
    </section>



<section class="why-choose-us-area  section-padding-80-0 clearfix mb-80" style="padding-top: 30px;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center wow fadeInUp  " data-wow-delay="100ms">
                        <h2>Our Service</h2>

                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Why Choose Area -->
                <div class="col-md-6 col-lg-4">
                    <div class="why-choose-us-content text-center  wow fadeInUp" data-wow-delay="100ms">
                        <!-- <div class="chosse-us-icon"> -->
                            <!-- <i class="fa fa-film" aria-hidden="true"></i> -->
                       <!--  </div> -->
                        <h4>Graphic Design</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut elit, sed do eiusmod te</p>
                    </div>
                </div>

                <!-- Single Why Choose Area -->
                <div class="col-md-6 col-lg-4">
                    <div class="why-choose-us-content text-center wow fadeInUp" data-wow-delay="300ms">
                        <!-- <div class="chosse-us-icon">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </div> -->
                        <h4>Social Media Marketing</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut elit, sed do eiusmod te</p>
                    </div>
                </div>

                <!-- Single Why Choose Area -->
                <div class="col-md-6 col-lg-4">
                    <div class="why-choose-us-content text-center  wow fadeInUp" data-wow-delay="500ms">
                        <!-- <div class="chosse-us-icon">
                            <i class="fa fa-camera" aria-hidden="true"></i>
                        </div> -->
                        <h4>Web Development</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut elit, sed do eiusmod te</p>
                    </div>
                </div>
            </div>

<div class="row">
                <!-- Single Why Choose Area -->
                <div class="col-md-6 col-lg-4">
                    <div class="why-choose-us-content text-center  wow fadeInUp" data-wow-delay="100ms">
                        <!-- <div class="chosse-us-icon"> -->
                            <!-- <i class="fa fa-film" aria-hidden="true"></i> -->
                       <!--  </div> -->
                        <h4>Graphic Design</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut elit, sed do eiusmod te</p>
                    </div>
                </div>

                <!-- Single Why Choose Area -->
                <div class="col-md-6 col-lg-4">
                    <div class="why-choose-us-content text-center wow fadeInUp" data-wow-delay="300ms">
                        <!-- <div class="chosse-us-icon">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </div> -->
                        <h4>Social Media Marketing</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut elit, sed do eiusmod te</p>
                    </div>
                </div>

                <!-- Single Why Choose Area -->
                <div class="col-md-6 col-lg-4">
                    <div class="why-choose-us-content text-center  wow fadeInUp" data-wow-delay="500ms">
                        <!-- <div class="chosse-us-icon">
                            <i class="fa fa-camera" aria-hidden="true"></i>
                        </div> -->
                        <h4>Web Development</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut elit, sed do eiusmod te</p>
                    </div>
                </div>
            </div>


  


        </div>
  


       


    </section>



    <!-- <section id="services" class="services section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title" style="padding-top: 30px;">
          <h2>Services</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row">
          <div class="col-xl-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class="fa fa-film"></i></div>
              <h4 style="text-align: center;"><a href="">Lorem Ipsum</a></h4>
              <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
               <a href="#" class="btn alime-btn2 mb-3 mb-sm-0 ">Viwe More</a>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class="fa fa-file"></i></div>
              <h4 style="text-align: center;"><a href="">Sed ut perspici</a></h4>
              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
              <a href="#" class="btn alime-btn2 mb-3 mb-sm-0 ">Viwe More</a>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="fa fa-tachometer"></i></div>
              <h4 style="text-align: center;"><a href="">Magni Dolores</a></h4>
              <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
               <a href="#" class="btn alime-btn2 mb-3 mb-sm-0 ">Viwe More</a>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="fa fa-film"></i></div>
              <h4 style="text-align: center;"><a href="">Nemo Enim</a></h4>
              <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
               <a href="#" class="btn alime-btn2 mb-3 mb-sm-0 ">Viwe More</a>
            </div>
          </div>

        </div>

      </div>
    </section> -->






    <!-- <section id="pricing" class="pricing  bg-gray">
      <div class="container " data-aos="fade-up">

        <div class="section-title" style="padding-top: 30px;">
          <h2>Pricing</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row" style="padding-bottom: 80px;">

          <div class="col-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div class="box">
              <h3>Free Plan</h3>
              <h4><sup>$</sup>0<span>per month</span></h4>
              <ul>
                <li><i class="fa fa-check"></i> Quam adipiscing vitae proin</li>
                <li><i class="fa fa-check"></i> Nec feugiat nisl pretium</li>
                <li><i class="fa fa-check"></i> Nulla at volutpat diam uteera</li>
                <li class="na"><i class="fa fa-x"></i> <span>Pharetra massa massa ultricies</span></li>
                <li class="na"><i class="fa fa-x"></i> <span>Massa ultricies mi quis hendrerit</span></li>
              </ul>
              <a href="#" class="buy-btn">Contat US</a>
            </div>
          </div>

          <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
            <div class="box featured">
              <h3>Business Plan</h3>
              <h4><sup>$</sup>29<span>per month</span></h4>
              <ul>
                <li><i class="fa fa-check"></i> Quam adipiscing vitae proin</li>
                <li><i class="fa fa-check"></i> Nec feugiat nisl pretium</li>
                <li><i class="fa fa-check"></i> Nulla at volutpat diam uteera</li>
                <li><i class="fa fa-check"></i> Pharetra massa massa ultricies</li>
                <li><i class="fa fa-check"></i> Massa ultricies mi quis hendrerit</li>
              </ul>
              <a href="#" class="buy-btn">Contat US</a>
            </div>
          </div>

          <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300">
            <div class="box">
              <h3>Developer Plan</h3>
              <h4><sup>$</sup>49<span>per month</span></h4>
              <ul>
                <li><i class="fa fa-check"></i> Quam adipiscing vitae proin</li>
                <li><i class="fa fa-check"></i> Nec feugiat nisl pretium</li>
                <li><i class="fa fa-check"></i> Nulla at volutpat diam uteera</li>
                <li><i class="fa fa-check"></i> Pharetra massa massa ultricies</li>
                <li><i class="fa fa-check"></i> Massa ultricies mi quis hendrerit</li>
              </ul>
              <a href="#" class="buy-btn">Contat US</a>
            </div>
          </div>

        </div>

      </div>
    </section> -->


    <!-- Welcome Area End -->

    <!-- Gallery Area Start -->

    <!-- Follow Area End -->

    <!-- Footer Area Start -->
    <?php include 'footer.php'; ?>
    <!-- Footer Area End -->
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v11.0" nonce="jyDnqK4j"></script>

    <!-- **** All JS Files ***** -->
    <!-- jQuery 2.2.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="js/alime.bundle.js"></script>
    <!-- Active -->
    <script src="js/default-assets/active.js"></script>

</body>


<body2>
  <div id="myDiv"></div>
</body2>
<script type="text/javascript">
  $(function () {
    $('#myDiv').floatingWhatsApp({
      phone: '5491133359850'
    });
  });
</script>


</html>